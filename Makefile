.PHONY: build test

build:
	mkdir -p build
	cp *.py build/
	echo "version = '`date -R`'" > build/version.py

test:
	python build/main.py
